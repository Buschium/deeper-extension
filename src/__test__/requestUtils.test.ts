import mockBrowser from "./__mocks__/mockBrowser";
import browser from "webextension-polyfill";

import {
    attachWebRequestListener,
    assessAndRedirectFunctionTemplate,
    startTimer
} from "../utils/requestUtils";
import { setTimestampEnd } from "../utils/storageUtils";

const allowlist = [/^https:\/\/(\S+\.)*example\.com\/path\/\S*(\/\S*)*$/]; // Matching pattern: "https://example.com/path/*/"
const allowedDestination = "https://www.example.com/path/example";
const blockedDestination = "https://org.test.com/blocked";

jest.mock("../utils/storageUtils");
jest.mock("webextension-polyfill", () => mockBrowser);

describe("Test Request Blocker", () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    describe("assessAndRedirectFunction Test", () => {
        const assessAndRedirectFunction =
            assessAndRedirectFunctionTemplate(allowlist);

        test("The assessAndRedirectFunctionTemplate returns a function", () => {
            expect(typeof assessAndRedirectFunction === "function").toBeTruthy;
        });

        test("It correctly assessses an allowed destination", () => {
            expect(
                assessAndRedirectFunction({ url: allowedDestination })
            ).toStrictEqual({});
        });

        test("It correctly assesses a blocked destination", () => {
            expect(
                assessAndRedirectFunction({ url: blockedDestination })
            ).not.toStrictEqual({});
        });

        test("It forwards a blocked request to the blockpage", () => {
            expect(
                assessAndRedirectFunction({ url: blockedDestination })
            ).toHaveProperty("redirectUrl");
        });
    });

    test("attachWebRequestListener attaches a listener", () => {
        const assessAndRedirectFunction = jest.fn();
        attachWebRequestListener(assessAndRedirectFunction);
        expect(
            browser.webRequest.onBeforeRequest.addListener
        ).toHaveBeenCalledTimes(1);
    });

    test("startTimer sets up an alarm and adds a listener for the end of the session", async () => {
        const timestampEnd = Date.now() + 1000 * 60 * 30; // 30 minutes from now
        const assessAndRedirectFunction = jest.fn();

        await startTimer(timestampEnd, assessAndRedirectFunction);

        expect(setTimestampEnd).toHaveBeenCalledWith(timestampEnd);
        expect(browser.alarms.onAlarm.addListener).toHaveBeenCalledWith(
            expect.any(Function)
        );
        expect(browser.alarms.create).toHaveBeenCalledWith(
            "deeper session timeout",
            { when: timestampEnd }
        );
    });
});
