import {
    isValidPattern,
    makeAllowlist,
    makeDomainRegex,
    makePathRegex,
    makeProtocolRegex,
    makeRegex
} from "../utils/matchingUtils";

describe("Pattern Validator Test", () => {
    test("It correctly validates protocol patterns", () => {
        // Valid protocols
        expect(isValidPattern("example.com")).toBe(true);
        expect(isValidPattern("*.example.com")).toBe(true);
        expect(isValidPattern("*://example.com")).toBe(true);
        expect(isValidPattern("https://example.com")).toBe(true);
        expect(isValidPattern("http://example.com")).toBe(true);
        expect(isValidPattern("ftp://example.com")).toBe(true);
        expect(isValidPattern("urn://example.com")).toBe(true);

        // Invalid protocols
        expect(isValidPattern("://example.com")).toBe(false);
        expect(isValidPattern("smp://example.com")).toBe(false);
        expect(isValidPattern("file:///example.com")).toBe(false);
        expect(isValidPattern("https:///example.com")).toBe(false);
        expect(isValidPattern("https:/example.com")).toBe(false);
        expect(isValidPattern("**://example.com")).toBe(false);
    });

    test("It correctly validates domain patterns", () => {
        // Valid domains
        expect(isValidPattern("example.com")).toBe(true);
        expect(isValidPattern("*.example.com")).toBe(true);
        expect(isValidPattern("*.example.co.uk")).toBe(true);
        expect(isValidPattern("www.example.com")).toBe(true);
        expect(isValidPattern("ex-ample.com")).toBe(true);
        expect(isValidPattern("*.com")).toBe(true);
        expect(isValidPattern("*")).toBe(true);
        expect(isValidPattern("example.*")).toBe(true);
        expect(isValidPattern("*.example.*")).toBe(true);

        // Invalid domains
        expect(isValidPattern(".example.com")).toBe(false);
        expect(isValidPattern("example.")).toBe(false);
        expect(isValidPattern("example")).toBe(false);
        expect(isValidPattern("*.*")).toBe(false);
        expect(isValidPattern("ex/ample.com")).toBe(false);
    });

    test("It correctly validates path patterns", () => {
        // Valid paths
        expect(isValidPattern("example.com/*")).toBe(true);
        expect(isValidPattern("example.com/path/to/resource")).toBe(true);
        expect(isValidPattern("example.com/path/to/resource/*")).toBe(true);
        expect(isValidPattern("example.com/path/*/resource")).toBe(true);
        expect(isValidPattern("example.com/path*")).toBe(true);
        expect(isValidPattern("example.com/*path")).toBe(true);

        // Invalid paths
        expect(isValidPattern("example.com\\*")).toBe(false);
        expect(isValidPattern("example.com/path\\to/resource")).toBe(false);
        expect(isValidPattern("example.com/path\\to/resource")).toBe(false);
    });
});

describe("Protocol Regex Factory Test", () => {
    test("It returns the correct regex when no protocol is given", () => {
        const re = new RegExp(makeProtocolRegex(undefined));
        expect(re).toEqual(/\S+:\/\//);
        expect(re.test("https://")).toBe(true);
        expect(re.test("fmp://")).toBe(true);
        expect(re.test("://")).toBe(false);
        expect(re.test("")).toBe(false);
    });

    test("It returns the correct regex when a wildcard protocol is given", () => {
        const re = new RegExp(makeProtocolRegex("*://"));
        expect(re).toEqual(/\S+:\/\//);
        expect(re.test("https://")).toBe(true);
        expect(re.test("fmp://")).toBe(true);
        expect(re.test("://")).toBe(false);
        expect(re.test("")).toBe(false);
    });

    test("It returns the correct regex when a protocol is given", () => {
        const re = new RegExp(makeProtocolRegex("https://"));
        expect(re).toEqual(/https:\/\//);
        expect(re.test("https://")).toBe(true);
        expect(re.test("http://")).toBe(false);
        expect(re.test("fmp://")).toBe(false);
        expect(re.test("://")).toBe(false);
        expect(re.test("")).toBe(false);
    });
});

describe("Domain Regex Factory Test", () => {
    test("It returns the correct regex when no subdomain is given", () => {
        const re = new RegExp(
            "^" + makeDomainRegex("example.com", false) + "$"
        );
        expect(re).toEqual(/^(\S+\.)*example\.com$/);
        expect(re.test("example.com")).toBe(true);
        expect(re.test("www.example.com")).toBe(true);
        expect(re.test("developers.example.com")).toBe(true);
        expect(re.test(".example.com")).toBe(false);
        expect(re.test("example.org")).toBe(false);
    });

    test("It returns the correct regex when a wildcard subdomain is given", () => {
        const re = new RegExp(
            "^" + makeDomainRegex("*.example.com", true) + "$"
        );
        expect(re).toEqual(/^(\S+\.)*example\.com$/);
        expect(re.test("example.com")).toBe(true);
        expect(re.test("www.example.com")).toBe(true);
        expect(re.test("developers.example.com")).toBe(true);
        expect(re.test(".example.com")).toBe(false);
        expect(re.test("example.org")).toBe(false);
    });

    test("It returns the correct regex when a subdomain is given", () => {
        const re = new RegExp(
            "^" + makeDomainRegex("mail.example.com", false) + "$"
        );
        expect(re).toEqual(/^(\S+\.)*mail\.example\.com$/);
        expect(re.test("mail.example.com")).toBe(true);
        expect(re.test("subdomain.mail.example.com")).toBe(true);
        expect(re.test("example.com")).toBe(false);
        expect(re.test("www.example.com")).toBe(false);
        expect(re.test("developers.example.com")).toBe(false);
        expect(re.test("example.org")).toBe(false);
    });

    test("It returns the correct regex when a wildcard domain is given", () => {
        const re = new RegExp("^" + makeDomainRegex("*", false) + "$");
        expect(re).toEqual(/^(\S+\.)+\S+$/);
        expect(re.test("example.com")).toBe(true);
        expect(re.test("www.example.com")).toBe(true);
        expect(re.test("mail.example.com")).toBe(true);
        expect(re.test("subdomain.mail.example.com")).toBe(true);
        expect(re.test("example.co.uk")).toBe(true);
    });
});

describe("Path Regex Factory Test", () => {
    test("It returns the correct regex when no path is given", () => {
        const re = new RegExp("^" + makePathRegex(undefined) + "$");
        expect(re).toEqual(/^(\/[a-zA-Z0-9-\._~\!\$&'\(\)\*\+,;:=@%\/]*)*$/);
        expect(re.test("/path")).toBe(true);
        expect(re.test("")).toBe(true);
        expect(re.test("/path/to/resource")).toBe(true);
        expect(re.test("/path\\to\\resource")).toBe(false);
    });

    test("It returns the correct regex when only a wildcard path is given", () => {
        const re = new RegExp("^" + makePathRegex("/*") + "$");
        expect(re).toEqual(/^(\/[a-zA-Z0-9-\._~\!\$&'\(\)\*\+,;:=@%\/]*)*$/);
        expect(re.test("/path")).toBe(true);
        expect(re.test("")).toBe(true);
        expect(re.test("/path/to/resource")).toBe(true);
        expect(re.test("/path\\to\\resource")).toBe(false);
    });

    test("It returns the correct regex when a path is given", () => {
        const re = new RegExp("^" + makePathRegex("/path/") + "$");
        expect(re).toEqual(
            /^\/path(\/[a-zA-Z0-9-\._~\!\$&'\(\)\*\+,;:=@%\/]*)*$/
        );
        expect(re.test("/path")).toBe(true);
        expect(re.test("/path/")).toBe(true);
        expect(re.test("/path/to/resource")).toBe(true);
        expect(re.test("")).toBe(false);
        expect(re.test("/pathtoanotherresource")).toBe(false);
        expect(re.test("/anotherpath")).toBe(false);
        expect(re.test("path\\to\\resource")).toBe(false);
    });

    test("It returns the correct regex when a path with wildcards is given", () => {
        const re = new RegExp("^" + makePathRegex("/path/*/resource") + "$");
        expect(re).toEqual(
            /^\/path\/[a-zA-Z0-9-\._~\!\$&'\(\)\*\+,;:=@%\/]*\/resource(\/[a-zA-Z0-9-\._~\!\$&'\(\)\*\+,;:=@%\/]*)*$/
        );
        expect(re.test("/path")).toBe(false);
        expect(re.test("/path/")).toBe(false);
        expect(re.test("/path/to/resource")).toBe(true);
        expect(re.test("/path/to/another/resource")).toBe(true);
        expect(re.test("")).toBe(false);
        expect(re.test("/pathtoanotherresource")).toBe(false);
        expect(re.test("/anotherpath")).toBe(false);
        expect(re.test("path\\to\\resource")).toBe(false);
    });
});

describe("URL Regex Factory Test", () => {
    test("It returns the correct regex for an URL", () => {
        const re = makeRegex("http://example.com/path/*/");
        expect(re.test("http://example.com/path/")).toBe(true);
        expect(re.test("http://example.com/path/anotherpath")).toBe(true);
        expect(re.test("http://www.example.com/path/subpath")).toBe(true);
        expect(re.test("http://mail.example.com/path/subpath")).toBe(true);

        expect(re.test("example.com/path")).toBe(false);
        expect(re.test("www.example.com/path")).toBe(false);
        expect(re.test("http://example.com")).toBe(false);
        expect(re.test("http://example.org/path")).toBe(false);
        expect(re.test("https://example.com/path")).toBe(false);
        expect(re.test("smp://example.com/path")).toBe(false);
        expect(re.test("http://example.domain.com/path")).toBe(false);
        expect(re.test("http://example.com/anotherpath")).toBe(false);
    });
});

describe("makeAllowlist Test", () => {
    const allowlist = makeAllowlist([
        "http://example.com/*",
        "mail.example.org"
    ]);
    const isAllowed = (url: string) =>
        allowlist.some((regex) => regex.test(url));

    test("It returns the correct regex list for a list of pattern URLs", () => {
        expect(isAllowed("http://example.com/")).toBe(true);
        expect(isAllowed("http://example.com/path")).toBe(true);
        expect(isAllowed("http://example.com/path/")).toBe(true);
        expect(isAllowed("http://example.com/path/anotherpath")).toBe(true);
        expect(isAllowed("http://www.example.com/path")).toBe(true);
        expect(isAllowed("http://mail.example.com/path")).toBe(true);
        expect(isAllowed("https://mail.example.org")).toBe(true);
        expect(isAllowed("http://mail.example.org")).toBe(true);
        expect(isAllowed("https://mail.example.org/mymail")).toBe(true);

        expect(isAllowed("example.com/path")).toBe(false);
        expect(isAllowed("www.example.com/path")).toBe(false);
        expect(isAllowed("http://example.org/path")).toBe(false);
        expect(isAllowed("https://example.com/path")).toBe(false);
        expect(isAllowed("smp://example.com/path")).toBe(false);
        expect(isAllowed("http://example.domain.com/path")).toBe(false);
        expect(isAllowed("https://images.example.org")).toBe(false);
    });

    test("It allows file paths", () => {
        expect(isAllowed("file:///C:/path/to/my/file")).toBe(true);
    });
});
