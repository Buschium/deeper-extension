import { mount } from "@vue/test-utils";
import sessionInactivePopup from "../components/sessionInactivePopup.vue";

describe("sessionInactivePopup Test", () => {
    const wrapper = mount(sessionInactivePopup);

    test("It displays the URL list textarea", () => {
        const urlListInput = wrapper.find("#urlListInput");
        expect(urlListInput.exists()).toBe(true);
        expect(urlListInput.element.tagName).toBe("TEXTAREA");
    });

    test("It displays the block duration input", () => {
        const blockDurationInput = wrapper.find("#blockDurationInput");
        expect(blockDurationInput.exists()).toBe(true);
        expect(blockDurationInput.element.tagName).toBe("INPUT");
    });

    test("It displays the submit button", () => {
        const startSessionButton = wrapper.find("#startSessionButton");
        expect(startSessionButton.exists()).toBe(true);
        expect(startSessionButton.element.tagName).toBe("BUTTON");
    });
});
