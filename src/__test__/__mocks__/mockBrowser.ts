export const alarms = {
    onAlarm: {
        addListener: jest.fn()
    },
    create: jest.fn()
};

export const webRequest = {
    onBeforeRequest: {
        removeListener: jest.fn(),
        addListener: jest.fn()
    }
};

export const storage = {
    local: {
        set: jest.fn(),
        get: jest.fn()
    }
};

export const runtime = {
    getURL: jest.fn()
};

const browser = {
    alarms,
    webRequest,
    storage,
    runtime
};

export default browser;
