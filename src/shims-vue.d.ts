declare module "*.vue" {
    import { sessionInactivePopup } from "vue";
    const component: sessionInactivePopup;
    export default component;
}
