import browser from "webextension-polyfill";

export const getAllowlist = () => browser.storage.local.get("allowlist");

export const setAllowlist = (allowlist: RegExp[]) =>
    browser.storage.local.set({ allowlist });

export const getTimestampEnd = () => browser.storage.local.get("timestampEnd");

export const setTimestampEnd = (timestampEnd: number) =>
    browser.storage.local.set({ timestampEnd });
