import browser from "webextension-polyfill";
import { setAllowlist, setTimestampEnd } from "./storageUtils";
import { makeAllowlist } from "./matchingUtils";
import { assessmentFunction } from "./types";

export async function startSession(timestampEnd: number, urlList: string[]) {
    console.log(
        `Starting session until ${new Date(timestampEnd)}. Duration: ${
            (timestampEnd - Date.now()) / 60_000
        } minutes.`
    );

    // Set up allowlist
    const allowlist = makeAllowlist(urlList);
    await setAllowlist(allowlist);

    // Set up assessment function
    const assessAndRedirectFunction =
        assessAndRedirectFunctionTemplate(allowlist);

    // Start blocking requests
    attachWebRequestListener(assessAndRedirectFunction);

    // Start the timer *after* we started blocking requests
    startTimer(timestampEnd, assessAndRedirectFunction);
}

export function attachWebRequestListener(
    assessAndRedirectFunction: assessmentFunction
) {
    browser.webRequest.onBeforeRequest.addListener(
        assessAndRedirectFunction,
        { urls: ["http://*/*", "https://*/*"], types: ["main_frame"] },
        ["blocking"]
    );
}

export const assessAndRedirectFunctionTemplate =
    (allowlist: RegExp[]) => (requestDetails: { url: string }) => {
        const isDestinationAllowed = allowlist.some((x) =>
            x.test(requestDetails.url)
        );

        return isDestinationAllowed
            ? {}
            : {
                  redirectUrl: browser.runtime.getURL(
                      "blockpage/blockpage.html"
                  )
              };
    };

export async function startTimer(
    timestampEnd: number,
    assessAndRedirectFunction: assessmentFunction
) {
    function endSession() {
        browser.webRequest.onBeforeRequest.removeListener(
            assessAndRedirectFunction
        );
    }

    await setTimestampEnd(timestampEnd);
    browser.alarms.onAlarm.addListener(endSession);
    browser.alarms.create("deeper session timeout", {
        when: timestampEnd
    });
}
