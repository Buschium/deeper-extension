export type assessmentFunction = (requestDetails: { url: string }) =>
    | {}
    | {
          redirectUrl: string;
      };
