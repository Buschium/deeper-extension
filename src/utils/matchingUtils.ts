const regexCache = new Map();

const validationRegex =
    /^(?<protocol>(?:\*|https|http|ftp|urn):\/\/)?(?<domain>(?<subdomain>\*\.)?[^\.\/\*]+[^\/\*]*\.([\d\w\*]{2,}|\*)|\*\.\S{2,}|\*)(?<path>\/[\w\d\*\/]*)?$/;

export const isValidPattern = (pattern: string) =>
    validationRegex.test(pattern);

export const makeProtocolRegex = (protocol: string | undefined) =>
    !protocol || protocol === "*://"
        ? "\\S+://"
        : protocol.replaceAll("/", "\\/");

export const makeDomainRegex = (domain: string, wildcardSubdomain: boolean) => {
    if (domain === "*") {
        return "(\\S+\\.)+\\S+";
    } else {
        return (
            "(\\S+\\.)*" +
            (wildcardSubdomain ? domain.slice(2) : domain)
                .replaceAll("*", "\\S*")
                .replaceAll(".", "\\.")
        );
    }
};

export const makePathRegex = (path: string | undefined) =>
    !path || path === "/*"
        ? "(\\/[a-zA-Z0-9-\\._~\\!\\$&'\\(\\)\\*\\+,;:=@%\\/]*)*"
        : (path.endsWith("/") ? path.slice(0, -1) : path)
              .replaceAll("/", "\\/")
              .replaceAll(
                  "*",
                  "[a-zA-Z0-9-\\._~\\!\\$$&'\\(\\)\\*\\+,;:=@%\\/]*"
              )
              .concat("(\\/[a-zA-Z0-9-\\._~\\!\\$&'\\(\\)\\*\\+,;:=@%\\/]*)*");

export function makeRegex(pattern: string): RegExp {
    if (regexCache.has(pattern)) {
        return regexCache.get(pattern);
    }

    const match = pattern.match(validationRegex);
    return new RegExp(
        [
            "^",
            makeProtocolRegex(match?.groups?.protocol),
            makeDomainRegex(
                match?.groups?.domain!,
                Boolean(match?.groups?.subdomain)
            ),
            makePathRegex(match?.groups?.path),
            "$"
        ].join("")
    );
}

export const makeAllowlist = (patternList: string[]) =>
    patternList.map((x) => makeRegex(x)).concat(/^file:\/\/\//); // Always allow local files
