export default {
    moduleFileExtensions: ["js", "ts", "mjs", "vue"],
    transform: {
        "^.+\\.ts$": [
            "ts-jest",
            {
                useESM: true
            }
        ],
        "^.+\\.vue$": "@vue/vue3-jest"
    },
    testEnvironment: "jsdom",
    testEnvironmentOptions: {
        customExportConditions: ["node", "node-addons"]
    },
    testMatch: ["**/src/__test__/*.test.ts?(x)"]
};
